package main

import (
	"io/ioutil"

	"encoding/json"

	log "github.com/Sirupsen/logrus"
)

type Config struct {
	DToken       string `json:"discord_token"`
	DBAdrr       string `json:"redis_address"`
	DBPass       string `json:"redis_password"`
	TwitchClient string `json:"twitch_clientID"`
	DataBaseType int    `json:"database_type"`
}

func LoadConfig(filename string) *Config {
	var config Config

	info, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error(err)
	}

	json.Unmarshal(info, &config)

	return &config
}
