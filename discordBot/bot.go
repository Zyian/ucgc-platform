package discordBot

import (
	"strings"

	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"gitlab.com/Zyian/ucgc-platform/discordBot/cmdModules"
	"gitlab.com/Zyian/ucgc-platform/discordBot/util"
)

var (
	botID    string
	session  *discordgo.Session
	err      error
	dbClient *redis.Client
)

//StartBot starts a new Discord Session
func StartBot(token, dbaddr, dbpass, twitchClient string, dbtype int) {

	if token == "" || dbaddr == "" || dbpass == "" {
		log.Fatal("Incorrect Parameters Given. Ensure You provide ALL parameters.")
	}

	dbClient = redis.NewClient(&redis.Options{
		Addr:     dbaddr,
		Password: dbpass,
		DB:       dbtype,
	})

	log.Info("Creating Discord Session:")
	session, err = discordgo.New("Bot " + token)
	if err != nil {
		log.Fatal("Error Creating Discord Session: ", err)
	}

	u, err := session.User("@me")
	if err != nil {
		log.Fatal("Error Cannot Obtain User Details: ", err)
	}

	session.AddHandler(cmdManager)
	botID = u.ID

	err = session.Open()
	if err != nil {
		log.Fatal("Error Opening Websocket Connection: ", err)
	}

	log.Info("Bot is now running")
	defaultsAdd()

	//Start Go routine to constantly check for Twitch Streams
	time.Sleep(2 * time.Second)
	go func() {
		log.Info("Started Monitoring Twitch Streams")
		cmdModules.TwitchClientID = twitchClient
		passes := 1
		for {
			log.Debug("Checking Status, Pass: ", passes)
			cmdModules.ConstantTwitch(session, dbClient)
			passes++
			time.Sleep(3 * time.Minute)
		}
	}()
}

//StopBot closes the seesion TODO: To be used later
func StopBot() {
	session.Close()
	log.Info("Bot Stopped")
}

func cmdManager(dSession *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.Bot || !mentionCheck(botID, message.Content) {
		return
	}

	//Split the message into the Invoker and Args
	msgCommand := cmdPrep(message.Content, message.Author)

	//Exit if there is no command to run
	if msgCommand.Empty {
		return
	}

	channel, err := dSession.State.Channel(message.ChannelID)
	if err != nil {
		log.Info(err)
	}

	guild, err := dSession.State.Guild(channel.GuildID)
	if err != nil {
		log.Info(err)
	}

	msgScope := newScope(dSession, guild, channel, message.Author, message.Message, dbClient, msgCommand)

	cmd, found := programMap[msgScope.Command.Invoker]
	if found {
		cmd(*msgScope)
	} else {
		msgScope.Session.ChannelMessageSend(msgScope.Channel.ID, "Sorry I don't know that command.")
	}
}

func mentionCheck(userID, message string) bool {
	mentionType1 := "<@!" + userID + ">"
	mentionType2 := "<@" + userID + ">"

	if strings.HasPrefix(message, mentionType1) || strings.HasPrefix(message, mentionType2) {
		return true
	}
	return false
}

//NewScope returns a new Context struct with all values of current session
func newScope(session *discordgo.Session, guild *discordgo.Guild, channel *discordgo.Channel, user *discordgo.User, msg *discordgo.Message, db *redis.Client, cmd *util.Command) *util.Scope {
	scope := new(util.Scope)
	scope.Session = session
	scope.Guild = guild
	scope.Channel = channel
	scope.User = user
	scope.Message = msg
	scope.Redis = db
	scope.Command = cmd

	return scope
}
