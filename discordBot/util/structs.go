package util

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
)

//Scope is the bundled package of commonly used aspects of discord API
type Scope struct {
	Session *discordgo.Session
	Guild   *discordgo.Guild
	Channel *discordgo.Channel
	User    *discordgo.User
	Message *discordgo.Message
	Redis   *redis.Client
	Command *Command
}

type Command struct {
	Invoker string
	Args    []string
	Query   string
	Empty   bool
	User    *discordgo.User
}

type Cmdmap struct {
	Cmds *map[string]func(Scope)
}

type CmdHolder func(Scope)

func CleanNames(input, old, new string) string {
	return strings.Replace(input, old, new, -1)
}
