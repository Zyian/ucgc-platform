package discordBot

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/Zyian/ucgc-platform/discordBot/cmdModules"
	"gitlab.com/Zyian/ucgc-platform/discordBot/util"
)

var programMap map[string]util.CmdHolder

func cmdPrep(msgContent string, user *discordgo.User) *util.Command {
	msgparts := strings.SplitN(strings.ToLower(msgContent), " ", 3)
	msgparts = msgparts[1:] //Slicing off the Mention

	if len(msgparts) < 1 {
		return &util.Command{
			Empty: true,
		}
	}

	if len(msgparts) == 1 {
		return &util.Command{
			Invoker: msgparts[0],
			Args:    nil,
			Query:   "",
			Empty:   false,
			User:    user,
		}
	}

	argsQuery := strings.Join(msgparts[1:], "") //Slicing off the command Invoker

	return &util.Command{
		Invoker: msgparts[0],
		Args:    strings.Split(argsQuery, " "),
		Query:   argsQuery,
		Empty:   false,
		User:    user,
	}

}

func addCmd(invoker string, action util.CmdHolder) {
	programMap[invoker] = action
}

func defaultsAdd() {
	tmpMap := make(map[string]util.CmdHolder)
	programMap = tmpMap

	//Fun Commands

	//Twitch Streaming Commands
	addCmd("setchan", cmdModules.SetStreamChan)
	addCmd("addstream", cmdModules.AddStreamer)
	addCmd("delstream", cmdModules.DelStreamer)
	addCmd("liststream", cmdModules.ListStreams)
	addCmd("manstream", cmdModules.ManualTest)

	//Game Taggings TODO: DISABLED FOR NOW
	//addCmd("iplay", cmdModules.TagGame)
	//addCmd("noplay", cmdModules.RemoveGame)
	//addCmd("addblacklist", cmdModules.AddBlackList)
	//addCmd("blacklist", cmdModules.ListBlackList)

	//Utility Commands
	addCmd("listroles", cmdModules.ListRoles)

}
