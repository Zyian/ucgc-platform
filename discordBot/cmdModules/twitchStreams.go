package cmdModules

import (
	"fmt"

	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"gitlab.com/Zyian/twitch-go"
	"gitlab.com/Zyian/ucgc-platform/discordBot/util"
)

const (
	streamSet    = "twitchUsers"    //Redis Set of Streamers to Parse
	streamStatus = "userStatus"     //Redis Map for Streamer Status
	streamerID   = "streamerMsgID"  //Redis Map for Streamer Status Message IDs
	streamChan   = "streamTextChan" //ID of designated streaming channel
)

//TwitchClientID - Global Variable to hold the Client ID for the Twitch API
var TwitchClientID string

//AddStreamer adds a streamer to the database list
func AddStreamer(scope util.Scope) {
	_, err := scope.Redis.SAdd(streamSet, scope.Command.Args[0]).Result()
	if err != nil {
		log.Error("Error adding User to database set: ", err)
	}

	result := fmt.Sprintf("%s has added the Twitch Stream: %s to tracking.", scope.User.Username, scope.Command.Query)
	log.Info(result)

	_, errMsg := scope.Session.ChannelMessageSend(scope.Channel.ID, result)
	if errMsg != nil {
		log.Error("AddStreamer()", errMsg)
	}
}

//DelStreamer removes a streamer from the database list
func DelStreamer(scope util.Scope) {
	_, err := scope.Redis.SRem(streamSet, scope.Command.Args[0]).Result()
	if err != nil {
		log.Error("DelStreamer()", err)
	}

	result := fmt.Sprintf("%s has removed the Twitch Stream: %s to tracking.", scope.User.Username, scope.Command.Query)
	log.Info(result)

	_, errMsg := scope.Session.ChannelMessageSend(scope.Channel.ID, result)
	if errMsg != nil {
		log.Error("DelStreamer()", errMsg)
	}
}

//ListStreams lists the streams the bot is tracking
func ListStreams(scope util.Scope) {
	var nameList string

	list, redisErr := scope.Redis.SMembers(streamSet).Result()
	if redisErr != nil {
		log.Error(redisErr)
	}

	for _, name := range list {
		nameList = nameList + name + "\n"
	}

	util.CleanNames(nameList, "_", "\\_")

	_, err := scope.Session.ChannelMessageSendEmbed(scope.Channel.ID, &discordgo.MessageEmbed{
		Author: &discordgo.MessageEmbedAuthor{
			Name: "Here are the Streams I am tracking:",
		},
		Description: nameList,
	})
	if err != nil {
		log.Error(err)
	}
}

//CheckLiveStatus uses the Twitch API to parse to see whether a channel is live or not
func CheckLiveStatus(dSession *discordgo.Session, dbClient *redis.Client) {
	twitchSession := twitchgo.NewClient(TwitchClientID)

	channelList, redisErr := dbClient.SMembers(streamSet).Result()
	if redisErr != nil {
		log.Error(redisErr)
	}

	//Convert Usernames to IDs
	idList := twitchSession.GetUserID(channelList)
	streamChanID, _ := dbClient.Get(streamChan).Result()

	//Parse API for channel status
	for _, userID := range idList {
		streamInfo := twitchSession.GetStreamByUser(userID)
		msgID, _ := dbClient.HGet(streamerID, userID).Result()
		if streamInfo == nil {
			dbClient.HSet(streamStatus, userID, false)
			if msgID != "nil" {
				dSession.ChannelMessageDelete(streamChanID, msgID)
				dbClient.HSet(streamerID, userID, "nil")
			}
			continue
		}

		dbClient.HSet(streamStatus, userID, true)
		if msgID == "nil" {
			msg, err := dSession.ChannelMessageSendEmbed(streamChanID, buildTwitchEmbeds(streamInfo))
			if err != nil {
				log.Error("Woops: ", err)
			}
			dbClient.HSet(streamerID, userID, msg.ID)
		}
	}
}

func buildTwitchEmbeds(stream *twitchgo.Stream) *discordgo.MessageEmbed {
	em := &discordgo.MessageEmbed{
		Color: 0x6441A4,
	}

	em.Fields = make([]*discordgo.MessageEmbedField, 0)

	em.Author = &discordgo.MessageEmbedAuthor{
		Name:    "A Streamer went Live!",
		IconURL: "https://cdn3.iconfinder.com/data/icons/happily-colored-snlogo/512/twitch.png",
	}

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "User",
		Value:  stream.Channel.DisplayName,
		Inline: true,
	})

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "Title",
		Value:  stream.Channel.Status,
		Inline: true,
	})

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "Game",
		Value:  stream.Game,
		Inline: true,
	})

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "Watch Now!",
		Value:  "[Twitch.tv](" + stream.Channel.URL + ")",
		Inline: true,
	})

	em.Thumbnail = &discordgo.MessageEmbedThumbnail{
		URL: stream.Channel.Logo,
	}

	return em
}

//ConstantTwitch reads the information from CheckLiveStatus and sends/deletes embeds accordingly
func ConstantTwitch(dSession *discordgo.Session, dbClient *redis.Client) {
	//Get the Channel ID designated to Twitch Streams
	streamID, _ := dbClient.Get(streamChan).Result()
	if streamID == "" {
		log.Info("Stream Channel Needs to be Set")
		return
	}

	//Ensure that the Channel Exists for the ID given
	_, err := dSession.State.Channel(streamID)
	if err != nil {
		log.WithField("Stream Channel ID ", streamID).Error(err)
		return
	}

	//Begin to check Twitch for live streams
	CheckLiveStatus(dSession, dbClient)
}

//ManualTest is used to test embeds by invoking a command to parse one stream.
func ManualTest(scope util.Scope) {
	twitchSession := twitchgo.NewClient(TwitchClientID) //Create a new Twitch API client

	chanResult := twitchSession.GetStreamByUser(scope.Command.Args[0])

	msg := buildTwitchEmbeds(chanResult)

	msgInfo, _ := scope.Session.ChannelMessageSendEmbed(scope.Channel.ID, msg)

	go func() {
		time.Sleep(10 * time.Second)
		scope.Session.ChannelMessageDelete(scope.Channel.ID, msgInfo.ID)
	}()
}

//SetStreamChan - Command to set the streaming channel for the Server
func SetStreamChan(scope util.Scope) {
	scope.Redis.Set(streamChan, scope.Channel.ID, -1)

	scope.Session.ChannelMessageSend(scope.Channel.ID, "Okay, this was set as the streaming channel.")
}
