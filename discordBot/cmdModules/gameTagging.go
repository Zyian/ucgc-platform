package cmdModules

import (
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"gitlab.com/Zyian/ucgc-platform/discordBot/util"
)

const (
	keyBlacklist = "roleBlackList"
	keyGames     = "gamesList"
)

//*******************************
//***** NEEDS WORK & MEETING ****
//*******************************

func ListRoles(scope util.Scope) {
	var roleList, roleListID []string

	guildRoles, _ := scope.Session.GuildRoles(scope.Guild.ID)

	for _, val := range guildRoles {
		roleList = append(roleList, val.Name)
		roleListID = append(roleListID, val.ID)
	}

	newmsg := listRoleEmbedBuilder(roleListID, roleList)

	scope.Session.ChannelMessageSendEmbed(scope.Channel.ID, newmsg)
}

func AddBlackList(scope util.Scope) {
	rolesAdded := make([]string, 0)

	for _, roleID := range scope.Command.Args {
		scope.Redis.SAdd(keyBlacklist, roleID)
		rolesAdded = append(rolesAdded, roleID)
	}

	scope.Session.ChannelMessageSendEmbed(scope.Channel.ID, &discordgo.MessageEmbed{
		Title:       "Roles Blacklisted!",
		Description: "The following roles have been blacklisted: \n" + strings.Join(rolesAdded, "\n"),
		Color:       0xf4cb42,
	})
}

func ListBlackList(scope util.Scope) {
	blackRoles, _ := scope.Redis.SMembers(keyBlacklist).Result()

	scope.Session.ChannelMessageSendEmbed(scope.Channel.ID, &discordgo.MessageEmbed{
		Title:       "BlackListed Roles:",
		Description: "The following roles are on the Blacklist: \n" + strings.Join(blackRoles, "\n"),
		Color:       0xe50000,
	})
}

func listRoleEmbedBuilder(roleID, rolelist []string) *discordgo.MessageEmbed {
	em := &discordgo.MessageEmbed{}
	em.Fields = make([]*discordgo.MessageEmbedField, 0)
	em.Author = &discordgo.MessageEmbedAuthor{
		Name: "List of Server Roles",
	}

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "Role Name",
		Value:  strings.Join(rolelist, "\n"),
		Inline: true,
	})

	em.Fields = append(em.Fields, &discordgo.MessageEmbedField{
		Name:   "Role ID",
		Value:  strings.Join(roleID, "\n"),
		Inline: true,
	})

	return em
}

func TagGame(scope util.Scope) {
	guildRoleIDMap := make(map[string]string)
	//guildAdminRoles := make(map[string]string)

	guildRoles, err := scope.Session.GuildRoles(scope.Guild.ID)
	if err != nil {
		log.WithField("Function:", "TagGame()").Error(err)
	}

	for _, role := range guildRoles {
		guildRoleIDMap[role.ID] = role.Name
	}

	user, _ := scope.Session.State.Member(scope.Guild.ID, scope.Message.Author.ID)
	for _, role := range user.Roles {
		log.Info(role)
	}

}

func RemoveGame(scope util.Scope) {
	guildRoles, err := scope.Session.GuildRoles(scope.Guild.ID)
	if err != nil {
		log.Error(err)
	}

	for _, roles := range guildRoles {
		for _, game := range scope.Command.Args {
			if strings.ToLower(roles.Name) == game {
				scope.Session.GuildMemberRoleRemove(scope.Guild.ID, scope.User.ID, roles.ID)
			}
		}
	}

	scope.Session.ChannelMessageSend(scope.Channel.ID, "Removed Roles")
}

func addGame(dbClient *redis.Client, abr, real string) {
	dbClient.HSet(keyGames, abr, real)
}

func AddDefaultGames(dbClient *redis.Client) {
	addGame(dbClient, "csgo", "csgo")
	addGame(dbClient, "cs", "csgo")
	addGame(dbClient, "wow", "wow")
	addGame(dbClient, "pubg", "pubg")
	addGame(dbClient, "tf2", "tf2")
	addGame(dbClient, "overwatch", "ow")
	addGame(dbClient, "ow", "ow")
	addGame(dbClient, "osu", "osu")
	addGame(dbClient, "osu!", "osu")
	addGame(dbClient, "showdown", "pkmshowdown")

}
