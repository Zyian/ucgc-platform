# EVERYTHING MOVED TO https://gitlab.com/ucgc/discord

# UCONN Gaming Club Platform
An expansion project of the UCGC Discord Bot, the project now consists of three parts:

* A Discord Management & Multimedia Bot
* A Webserver serving a website to provide a centrum for the club
* A Twitch Chat bot to moderator for UCONN Esports affiliates

## Usage
There are two ways of starting the bot:

1) Using argument flags
    * ```go run main.go -t <Discord API token> -db <Redis Server address> -dp <Redis Auth Password>```
2) Use config.json
    * For this type you have to create a `config.json` file in the same directory as `main.go`. The basic structure goes as follows
    * ```json
        {
            "discord_token": "<token here>",
            "redis_address": "<address here>",
            "redis_password": "<password here>"
        } 
        ```
