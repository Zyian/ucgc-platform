package main

import (
	"flag"

	log "github.com/Sirupsen/logrus"
	"github.com/rifflock/lfshook"
	"gitlab.com/Zyian/ucgc-platform/discordBot"
)

func init() {
	log.SetLevel(log.DebugLevel)
	log.AddHook(lfshook.NewHook(lfshook.PathMap{
		log.InfoLevel:  "dev.log",
		log.ErrorLevel: "dev.log",
		log.FatalLevel: "dev.log",
		log.PanicLevel: "dev.log",
	}))
}

func main() {

	var (
		Token  = flag.String("t", "", "Discord Auth Token")
		DBAddr = flag.String("db", "", "Redis Database Address")
		DBPass = flag.String("dp", "", "Redis Database Password")
		DBType = flag.Int("dbtype", -1, "Redis Database Type")
		config Config
	)
	flag.Parse()

	if *Token == "" && *DBAddr == "" && *DBPass == "" {
		log.Info("Loading from Config file")
		config = *LoadConfig("config.json")
	} else {
		config.DToken = *Token
		config.DBAdrr = *DBAddr
		config.DBPass = *DBPass
		config.DataBaseType = *DBType
	}

	discordBot.StartBot(config.DToken, config.DBAdrr, config.DBPass, config.TwitchClient, config.DataBaseType)

	<-make(chan struct{})

}
